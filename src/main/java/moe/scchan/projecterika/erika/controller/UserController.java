package moe.scchan.projecterika.erika.controller;

import jakarta.annotation.Resource;
import moe.scchan.projecterika.erika.domain.User;
import moe.scchan.projecterika.erika.service.UserService;
import moe.scchan.projecterika.erika.utility.Result;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/user")
public class UserController {
    @Resource
    private UserService userService;

    @PostMapping("/login")
    @ResponseBody
    public Result<User> loginController(@RequestBody User requestUser) {
        User user = userService.loginService(requestUser.getUname(), requestUser.getPassword());
        if(user != null) {
            return Result.success(user,"Success");
        } else {
            return Result.error("11401","Username or password error");
        }
    }

    @PostMapping("/register")

    public Result<User> registerController(@RequestBody User newUser) {
        User user = userService.registerService(newUser);
        if(user != null) {
            return Result.success(user,"Success");
        } else {
            return Result.error("11402","User already exists");
        }
    }
}
