package moe.scchan.projecterika.erika.service;

import moe.scchan.projecterika.erika.domain.User;

public interface UserService {
    User loginService(String uname, String password);
    User registerService(User user);
}
