package moe.scchan.projecterika.erika.service.serviceImpl;

import jakarta.annotation.Resource;
import moe.scchan.projecterika.erika.domain.User;
import moe.scchan.projecterika.erika.repository.UserDao;
import moe.scchan.projecterika.erika.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;
    @Override
    public User loginService(String uname, String password) {
        User user = userDao.findByUnameAndPassword(uname, password);
        if(user != null){
            user.setPassword("");
        }
        return user;
    }

    @Override
    public User registerService(User user) {
        if(userDao.findByUname(user.getUname())!=null){
            return null;
        }else{
            User newUser = userDao.save(user);
            newUser.setPassword("");
            return newUser;
        }
    }
}
